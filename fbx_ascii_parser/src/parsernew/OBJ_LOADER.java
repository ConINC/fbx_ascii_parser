package parsernew;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

class GroupedMesh_String
{    
    public ArrayList<String> lines;
    
    public GroupedMesh_String()
    {
        lines = new ArrayList<>();
    }    
}



public class OBJ_LOADER
{
    //Just open the file
    public static ArrayList<String> open_Obj(String path) throws FileNotFoundException, IOException
    {        
        
        long starttime = System.nanoTime();
        System.out.println("Entering open_OBJ");                     
        ArrayList<String> obj_lines = (ArrayList<String>)Files.readAllLines(Paths.get(path));        
        ArrayList<String> obj_lines_new = new ArrayList<>();
                
        for(String line : obj_lines)             
        {            
            if(!(line.contains("g default") || line.startsWith("s ")))
            {
                obj_lines_new.add(line);                
            }            
        }                
        obj_lines.clear();        
                
        //Start loading the model
        OBJ_Model model = create_Model(obj_lines_new, starttime);    
        
        
        return obj_lines;
    }
    
    public static OBJ_Model create_Model(ArrayList<String> list, long time)
    {
        OBJ_Model model = new OBJ_Model();
        
        System.out.println("Entering grouped string creation");
        
        ArrayList<GroupedMesh_String> groups = new ArrayList<>();
        
        ArrayList<String> pointlines = new ArrayList<String>();
        ArrayList<String> vnLines = new ArrayList<String>();
        ArrayList<String> vtLines = new ArrayList<String>();        
        ArrayList<String> list_reduced = new ArrayList<>();
        
        String materialPath = "";   
        
        for(int i = 0; i < list.size(); i++)
        {
            
            if(list.get(i).startsWith("mtllib"))
            {
                materialPath = list.get(i).substring(7);
                continue;
            }
            if(list.get(i).startsWith("#") || list.get(i).length() < 2)
            {
                continue;
            }
            if(list.get(i).startsWith("v "))
            {
                pointlines.add(list.get(i));                
            }
            else if(list.get(i).startsWith("vt "))
            {
                vtLines.add(list.get(i));                
            }
            else if(list.get(i).startsWith("vn "))
            {
                vnLines.add(list.get(i));                
            }
            else
            {
                list_reduced.add(list.get(i));
            }
        }     
                      
        int counter = 0;
        int group_counter = 0;
        
        GroupedMesh_String gm_s = new GroupedMesh_String();        
        groups.add(gm_s);
        
        System.out.println(list_reduced.size());
           
        while(counter < list_reduced.size())
        {            
            groups.get(group_counter).lines.add(list_reduced.get(counter));            
            
            if(counter+1 >= list_reduced.size())
            {                
                break;
            }                        
            if(list_reduced.get(counter).startsWith("f ") && list_reduced.get(counter+1).startsWith("g "))
            {                                
                group_counter++;
                groups.add(new GroupedMesh_String());               
            }            
            counter++;
        }
        
        System.out.println(groups.size());
        System.out.println(materialPath);
        
        ArrayList<Vector2> uvs = makeVector2(vtLines,3);
        ArrayList<Vector3> points = makeVector3(pointlines,2);
        ArrayList<Vector3> vns = makeVector3(vnLines,3);
                
        System.out.println("Making Model now");
               
        ArrayList<Group> grouped_meshes =  get_fullGroups(groups);
        
        model.material_path = materialPath;                
        model.vertex_data = new Vertex_Data();
        model.vertex_data.Uvs = uvs;
        model.vertex_data.Normals = vns;
        model.vertex_data.Points = points;        
        model.group_data = grouped_meshes;
        
         long elapsedTime = (System.nanoTime() - time) / 1000000;
        
        System.out.println(elapsedTime);

        //now fill the new model format and save it, then repeat...
        //End here for now
        
        return model;
    }
    
    //get groups as string lines first
    public static ArrayList<Group> get_fullGroups(ArrayList<GroupedMesh_String> groups_as_lines)
    {        
        ArrayList<Group> groups = new ArrayList<>();
        
        for(GroupedMesh_String group_as_lines : groups_as_lines)
        {            
            //Create Faces from single group
            groups.add(createFaces_from_groupAsLines(group_as_lines));
        }
        
        return groups;
    }
    
    //Make groups with faces using indices including shadernames and groupnames
    public static Group createFaces_from_groupAsLines(GroupedMesh_String lines)
    {
        Group group = new Group();
        
        for(int i = 0; i < lines.lines.size(); i++)
        {
            
            if(lines.lines.get(i).startsWith("g "))
                group.GroupName = lines.lines.get(i).substring(2);
            
            if(lines.lines.get(i).startsWith("usemtl ")){
                group.faces.add(new Face());
                group.faces.get(group.faces.size()-1).appliedShader = lines.lines.get(i).substring(7);
            }
            
            if(lines.lines.get(i).startsWith("f "))
            {
                String[] tripplet = lines.lines.get(i).substring(2).split(" ");
                
                String[] A;
                String[] B;
                String[] C;
                
                A = tripplet[0].split("/");
                B = tripplet[1].split("/");
                C = tripplet[2].split("/");
                
                //Structure is:
                //f  1/1/1 2/2/2 3/3/3
                //each tripplet is given in  point index, uv index, normal index
                // 3 tripplets make a triangle, 1 tripplet makes a vertex
                int len = A.length;
                int[] pos_indices = new int[len];
                int[] normal_indices = new int[len];
                int[] vt_indices = new int[len];
                
                pos_indices[0] = Integer.parseInt(A[0]);
                pos_indices[1] = Integer.parseInt(B[0]);
                pos_indices[2] = Integer.parseInt(C[0]);
                                
                vt_indices[0] = Integer.parseInt(A[1]);
                vt_indices[1] = Integer.parseInt(B[1]);
                vt_indices[2] = Integer.parseInt(C[1]);
                                
                normal_indices[0] = Integer.parseInt(A[2]);
                normal_indices[1] = Integer.parseInt(B[2]);
                normal_indices[2] = Integer.parseInt(C[2]);
                
                group.faces.get(group.faces.size()-1).point_indices.add(pos_indices[0]);
                group.faces.get(group.faces.size()-1).point_indices.add(pos_indices[1]);
                group.faces.get(group.faces.size()-1).point_indices.add(pos_indices[2]);
                
                group.faces.get(group.faces.size()-1).uv_indices.add(vt_indices[0]);
                group.faces.get(group.faces.size()-1).uv_indices.add(vt_indices[1]);
                group.faces.get(group.faces.size()-1).uv_indices.add(vt_indices[2]);
                
                group.faces.get(group.faces.size()-1).vn_indices.add(normal_indices[0]);
                group.faces.get(group.faces.size()-1).vn_indices.add(normal_indices[1]);
                group.faces.get(group.faces.size()-1).vn_indices.add(normal_indices[2]);                
            }                       
        }        
        return group;        
    }
    
    //Makevector functions
    //<editor-fold>
    public static ArrayList<Vector2> makeVector2(ArrayList<String> uv_lines, int substring)
    {
        ArrayList<Vector2> uvs = new ArrayList<>();
        
        for(String line : uv_lines)
        {
            String[] splits = line.substring(substring).split(" ");
            Vector2 vec = new Vector2(Float.parseFloat(splits[0]),Float.parseFloat(splits[1]));        
            
            uvs.add(vec);
        }        
        
        return uvs;
    }    
    
    public static ArrayList<Vector3> makeVector3(ArrayList<String> point_lines, int substring)
    {
        ArrayList<Vector3> points = new ArrayList<>();
        
        for(String line : point_lines)
        {
            String thisline = line.substring(substring);
            try{
            String[] splits = line.substring(substring).split(" ");
            System.out.println(splits);
            Vector3 vec = new Vector3(Float.parseFloat(splits[0]),Float.parseFloat(splits[1]),Float.parseFloat(splits[2]));        
            
            points.add(vec);
            }
            catch(Exception e)
            {
                System.out.println("ERROR!" + thisline);
            }
        }        
        return points;
    }    
    //</editor-fold>
}
