package parsernew;
import java.util.ArrayList;
import java.util.List;

//Following are the Datatypes for the Binary format, which will be exported later

class PoseNode_string
{
    ArrayList<String> poseNodeLines;    
    public PoseNode_string()
    {
        poseNodeLines = new ArrayList<>();
    }
}
 class PoseNode
{
    public String nodeName;
    public ArrayList<Float> matrix;    
    public PoseNode()
    {
        nodeName = "";
        matrix = new ArrayList<>();
    }
}

class Joint_Animation_data
{
    public String jointName = "";
    public ArrayList<Float> translate_x = new ArrayList<>();
    public ArrayList<Long> translate_x_key = new ArrayList<>();
    
    public ArrayList<Float> translate_y = new ArrayList<>();   
    public ArrayList<Long> translate_y_key = new ArrayList<>();
    
    public ArrayList<Float> translate_z = new ArrayList<>();
    public ArrayList<Long> translate_z_key = new ArrayList<>();
    
    
    public ArrayList<Float> rotate_x = new ArrayList<>();
    public ArrayList<Long> rotate_x_key = new ArrayList<>();
    
    public ArrayList<Float> rotate_y = new ArrayList<>();
    public ArrayList<Long> rotate_y_key = new ArrayList<>();
    
    public ArrayList<Float> rotate_z = new ArrayList<>();
    public ArrayList<Long> rotate_z_key = new ArrayList<>();
    
    
    public ArrayList<Float> scale_x = new ArrayList<>();
    public ArrayList<Long> scale_x_key = new ArrayList<>();
    
    public ArrayList<Float> scale_y = new ArrayList<>();
    public ArrayList<Long> scale_y_key = new ArrayList<>();
    
    public ArrayList<Float> scale_z = new ArrayList<>();
    public ArrayList<Long> scale_z_key = new ArrayList<>();
    
    int begin_frame, end_frame;    
}

class Connection
{
    String parent, child;
}

class Shader
{
    String relTextureName = "";
}

class Mesh_Lines_Packed
{
    public String mesh_name = "";
    ArrayList<String> verticeLines = new ArrayList<>();
    ArrayList<String> vindex_lines = new ArrayList<>();
    ArrayList<String> normals_lines = new ArrayList<>();
    ArrayList<String> uv_lines = new ArrayList<>();
    ArrayList<String> uv_index_lines = new ArrayList<>();    
}

class DeformerNodes_String
{
    ArrayList<String> lines;
    
    public DeformerNodes_String()
    {
        lines = new ArrayList<>();
    }
}

class Deformer
{
    ArrayList<Integer> indexes;
    ArrayList<Float>  weights;
    String DeformerName;
    ArrayList<Float> Transform;
    ArrayList<Float> TransformLink;
    
    public Deformer()
    {
       indexes = new ArrayList<Integer>();
       weights = new ArrayList<>();
       DeformerName = ""; 
       Transform = new ArrayList<>();
       TransformLink = new ArrayList<>();
    }    
}

class Mesh_data_static
{
    String meshName = "";
    ArrayList<Float> points = new ArrayList<>();
    ArrayList<Integer> p_index = new ArrayList<>();
    ArrayList<Float> uv = new ArrayList<>();
    ArrayList<Integer> uv_index = new ArrayList<>();
    ArrayList<Float> normals = new ArrayList<>();    
}

class TextureData
{
    ArrayList<String> relfilenames = new ArrayList<>();    
}

class Animated_Deformer_string
{
   public List<String> lines = new ArrayList<>();
}

class Channel_String
{
    public ArrayList<String> lines;
    public Channel_String()
    {
        lines = new ArrayList<String>();        
    }
}

class SubChannel_String
{
    public ArrayList<String> lines;
    public SubChannel_String()
    {
        lines = new ArrayList<String>();
        
    }
}

//I will only use Transform channel, thats why i leave it out k?
class Animated_Deformer
{
   public String DeformerName;
   public Channel channel_Translate;
   public Channel channel_Rotate;
   public Channel channel_Scale; 
}

// Translate, Rotate, or Scale channel, containing 3 subchannels for each dimension
class Channel
{
    public SubChannel channel_X;
    public SubChannel channel_Y;
    public SubChannel channel_Z;
    
    public Channel()
    {
        channel_X = new SubChannel();
        channel_Y = new SubChannel();
        channel_Z = new SubChannel();
    }
}

class SubChannel
{
    public ArrayList<Long> keyDistances;
    public ArrayList<Float> keyValue;
    
    public SubChannel()
    {       
        keyDistances = new ArrayList<>();
        keyValue = new ArrayList<>();
    }
}

class KeyVal_Box
{
   public  ArrayList<Float> values = new ArrayList<>();
   public ArrayList<Long> keys = new ArrayList<>();
}
