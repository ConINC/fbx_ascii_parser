package parsernew;
import com.sun.deploy.util.SyncFileAccess;
import static java.awt.SystemColor.text;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.BufferedWriter;  
import java.nio.charset.Charset;
import java.nio.file.StandardOpenOption;
import java.util.*;
import org.omg.IOP.ENCODING_CDR_ENCAPS;

public class FBX_LOADER
{
    //Just open the file
    public static ArrayList<String> open_Fbx(String path) throws FileNotFoundException, IOException
    {
        long time = System.nanoTime();
        
        System.out.println("Entering open_FBX");
        ArrayList<String> fbx_lines = (ArrayList<String>)Files.readAllLines(Paths.get(path));
        ArrayList<String> fbx_lines_new = new ArrayList<>();
        ArrayList<String> Model_Mesh_lines = new ArrayList<>();
        
        boolean start_adding = false;
        int scopeCounter = 0;
        for(String line : fbx_lines)
        {
            String newline = line.trim();
            if(newline.startsWith(";"))
                continue;
            
            if(newline.contains("Model") && newline.contains(" \"Mesh\"") && newline.contains("{"))
            {
                start_adding = true;
                Model_Mesh_lines.add(newline);      
                scopeCounter++;
                continue;
            }
            
            if(start_adding && scopeCounter > 0)
            {
                Model_Mesh_lines.add(newline);      
                if(newline.contains("{"))
                    scopeCounter++;
                
                if(newline.contains("}"))
                    scopeCounter--;
            }
        }
        
        //Functions to recieve the string lines
        ArrayList<PoseNode_string> string_pose_Nodes = fbx_get_bindPoselines(fbx_lines, time); //DONE
        ArrayList<String> string_defor_nodes  = fbx_get_deformers(fbx_lines, time);  //not Done
        ArrayList<DeformerNodes_String> nodes_string_sorted  = get_deformer_nodes(string_defor_nodes);
        ArrayList<String> fbx_get_connections = fbx_get_connections(fbx_lines, time); //DONE
        ArrayList<String> fbx_get_takes = fbx_get_takes(fbx_lines, time); // not Done
        ArrayList<String> fbx_get_texture = fbx_get_texture(fbx_lines, time); //Half Done
        Mesh_Lines_Packed modelmeshpack = fbx_createModelMeshLines(Model_Mesh_lines, time);          
        //isolate deformer nodes
        ArrayList<DeformerNodes_String> deformerNodes_seperated = get_deformer_nodes(string_defor_nodes);
        ArrayList<Animated_Deformer> animated_deformers_done = new ArrayList<Animated_Deformer>();
        //Isolate deformers in animation
        Mesh_data_static staticmodel = new Mesh_data_static();
        ArrayList<PoseNode> poseNodes_model = new ArrayList<>();
        Shader curTextureInfo = new Shader(); 
        ArrayList<Connection> connections = new ArrayList<>();
        ArrayList<Deformer> deformers = new ArrayList<>();
        
        try
        {
            animated_deformers_done = get_Animated_Deformers_string(fbx_get_takes);
        }
        catch(Exception e)
        {
            System.out.println("Failed to parse animation, or noone included");
        }
        
        try
        {
            //Model is being assembled here
            staticmodel =  createMeshDataStatic(modelmeshpack);
            modelmeshpack = null; //remove this since we dont need it anymore
        }
        catch(Exception e)
        {
            System.out.println("An error occured parsing numbers in staticModel creation");
        }
        
        //Assign the pose nodes
        try
        {
            poseNodes_model = createPoseNodes(string_pose_Nodes);
            string_pose_Nodes = null;
        }
        catch(Exception e)
        {
            System.out.println("An error occured parsing numbers in posenodes creation");
        }
        
        //create texture info part   
        try
        {
            //Model is being assembled here        
            curTextureInfo = getTextureInfo(fbx_get_texture);
            fbx_get_texture = null;       
        }
        catch(Exception e)
        {
            System.out.println("An error occured parsing numbers in textureInfo creation");
        }
        
        //make connections
        try
        {
            connections = createConnections(fbx_get_connections);
            fbx_get_connections = null;
        }
        catch(Exception e)
        {
            System.out.println("An error occured parsing numbers in connections creation");
        }
        
        deformers =  make_deformers(deformerNodes_seperated);
        deformerNodes_seperated = null;
                
        long t = (System.nanoTime() - time) / 1000000;
        System.out.println(t);
        
        //Write_binary_file(staticmodel,poseNodes_model,curTextureInfo, connections, deformers,animated_deformers_done);
        String thepath =  "C://Users//Conex//Documents//Projekte//ProjektDaten//bluegem_game_project//game_resource//";
        
        /*
        try
        {
            System.out.println("writing staticmodel");
            write_mesh_as_text(staticmodel, curTextureInfo, thepath, "test.mesh");
        }        
        catch(IOException e)
        {
            System.out.println("Failed");
        }
        
        try
        {
            System.out.println("writing Connections");
            write_connections_as_text(connections, thepath, "test.connections");
        }
        catch(IOException e)
        {
            System.out.println("Failed");
        }
        
        System.out.println("writing Deformers");
        write_deformers_as_text(deformers, thepath, "test.deformer");
                
        try
        {            
            System.out.println("writing Pose");
            write_posenode_as_text(poseNodes_model, thepath, "test.pose");
        }
        catch(IOException e)
        {
            System.out.println("Failed");
        }
        */
        
        try
        {
            write_animation_as_text(animated_deformers_done, thepath, "test.animation");
        }
        catch(IOException e)
        {
            System.out.println("Failed");
        }
        
        return Model_Mesh_lines;
    }
    
    public static boolean write_mesh_as_text(Mesh_data_static staticmodel, Shader curTextureInfo, String path,  String name) throws FileNotFoundException, IOException
    {        
        
        if(staticmodel.points.size() > 0 && staticmodel.p_index.size() > 0 && staticmodel.normals.size() > 0
            && staticmodel.uv.size() > 0 && staticmodel.uv_index.size() > 0)
        {                  
            String full = "";
            String meshname = "meshname: " + staticmodel.meshName + "\n";
            String texture_path = "texturename: " + curTextureInfo.relTextureName+ "\n";
            
            full += meshname + texture_path;
            
            full += "<POINTS>\n";                     
            for(int i =0; i < staticmodel.points.size(); i++)
            {
                full += (Float.toString( staticmodel.points.get(i))+ "\n");
            }
            full += "</POINTS>\n";
            
            full += "<POINTI>\n";
            for(int i =0; i < staticmodel.p_index.size(); i++)
            {
                full += (Integer.toString(staticmodel.p_index.get(i))+ "\n");
            }
            full += "</POINTI>\n";
            
            full += "<NORMAL>\n";
            for(int i =0; i < staticmodel.normals.size(); i++)
            {
                full += (Float.toString(staticmodel.normals.get(i))+ "\n");
            }
            full += "</NORMAL>\n";
            
            full += "<UV>\n";
            for(int i =0; i < staticmodel.uv.size(); i++)
            {
                full += (Float.toString(staticmodel.uv.get(i))+ "\n");
            }
            full += "</UV>\n";
            
            full += "<UVI>\n";
            for(int i = 0; i < staticmodel.uv_index.size(); i++)
            {
                full += (Integer.toString(staticmodel.uv_index.get(i))+ "\n");
            }            
            full += "</UVI>\n";
           

                    
            System.out.println("Finished writing ascii based file");
                        
            Path logFile = Paths.get(path + name);            
            BufferedWriter writer = Files.newBufferedWriter(
                    logFile, 
                    StandardCharsets.UTF_8,
                    StandardOpenOption.CREATE);
            writer.write(full);
            writer.close();
            
            return true;
        }
        else
        {
            return false;
        }
    }        
    
    public static boolean write_connections_as_text(ArrayList<Connection> connections, String path,  String name) throws FileNotFoundException, IOException
    {
        String full = "";
        
        for(int i = 0; i < connections.size(); i++)
        {               
            //REMOVE NON JOINT NAMES
            String p = "", c = "";
            if(connections.get(i).parent.contains("Model::"))
            {
                p = connections.get(i).parent.substring(7);
                c = connections.get(i).child.substring(7);
            }
            
            if(connections.get(i).parent.contains("Material::"))
            {
                p = connections.get(i).parent;
                c = connections.get(i).child;
            }
            if(connections.get(i).parent.contains("Texture::"))
            {
                p = connections.get(i).parent;
                c = connections.get(i).child;
            }
            if(connections.get(i).parent.contains("Video::"))
            {
                p = connections.get(i).parent;
                c = connections.get(i).child;
            }            
            
            full += c + " " + p + "\n";
        }
        Path logFile = Paths.get(path+name);            
        BufferedWriter writer = Files.newBufferedWriter(
                    logFile, 
                    StandardCharsets.UTF_8,
                    StandardOpenOption.CREATE);
        writer.write(full);
        writer.close();
        
        return true;
    }       

    public static boolean write_deformers_as_text(ArrayList<Deformer> deformers, String path,  String name) throws FileNotFoundException, IOException
    {
        String full = "";
        int count = deformers.size();
        
        for(int i = 0; i < count; i++)
        {
            System.out.println(deformers.get(i).DeformerName);
            
            String deformer_name = "";
            if(deformers.get(i).DeformerName.contains("SubDeformer::Cluster_"))
            {                
                full += "<DEFORMER>\nNAME: " +  deformers.get(i).DeformerName.substring(21) + "\n";
            }
            else
                continue;
            
            full += "<TRANSFORM>\n";
            for(int x = 0; x < deformers.get(i).Transform.size(); x++)
            {
                full += Float.toString(deformers.get(i).Transform.get(x))+ "\n";
            }
            full += "</TRANSFORM>\n";
            
            full += "<TRANSFORMLINK>\n";
            for(int x = 0; x < deformers.get(i).TransformLink.size(); x++)
            {
                full += Float.toString(deformers.get(i).TransformLink.get(x))+ "\n";
            }
            full += "</TRANSFORMLINK>\n";
            
            
            full += "<WEIGHT>\n";
            for(int x = 0; x < deformers.get(i).weights.size(); x++)
            {
                full += Float.toString(deformers.get(i).weights.get(x))+ "\n";
            }
            full += "</WEIGHT>\n";
            
            full += "<INDEX>\n";
            for(int x = 0; x < deformers.get(i).indexes.size(); x++)
            {
                full += Integer.toString( deformers.get(i).indexes.get(x)) + "\n";
            }
            full += "</INDEX>\n";     
            full += "</DEFORMER>\n";
        }
                                
                
        Path logFile = Paths.get(path+name);            
        BufferedWriter writer = Files.newBufferedWriter(
                    logFile, 
                    StandardCharsets.UTF_8,
                    StandardOpenOption.CREATE);
        writer.write(full);
        writer.close();
            
        return true;
    }
    
    public static boolean write_animation_as_text(ArrayList<Animated_Deformer> anim_deformers, String path,  String name)throws FileNotFoundException, IOException
    {
        String full = "";
        int count = anim_deformers.size();
    
        for(int i = 0; i < count; i++)
        {
            full += "<JOINT>\n";
            
            String thename = anim_deformers.get(i).DeformerName.substring(7);
            full += "NAME: " + thename + "\n";
            
            
            //FOR TRANSLATION
            
            //FOR ALL X VALUES IN TRANSFORM
            full += "<TRANS_X_VAL>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Translate.channel_X.keyValue.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Translate.channel_X.keyValue.get(j)) + "\n";
            }
            full += "</TRANS_X_VAL>\n";
            
            full += "<TRANS_X_DIS>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Translate.channel_X.keyDistances.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Translate.channel_X.keyDistances.get(j)) + "\n";
            }
            full += "</TRANS_X_DIS>\n";
            
            //FOR ALL Y VALUES IN TRANSFORM
            full += "<TRANS_Y_VAL>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Translate.channel_Y.keyValue.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Translate.channel_Y.keyValue.get(j)) + "\n";
            }
            full += "</TRANS_Y_VAL>\n";
            
            full += "<TRANS_Y_DIS>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Translate.channel_Y.keyDistances.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Translate.channel_Y.keyDistances.get(j)) + "\n";
            }
            full += "</TRANS_Y_DIS>\n";
            
            //FOR ALL Z VALUES IN TRANSFORM
            full += "<TRANS_Z_VAL>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Translate.channel_Z.keyValue.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Translate.channel_Z.keyValue.get(j)) + "\n";
            }
            full += "</TRANS_Z_VAL>\n";
            
            full += "<TRANS_Z_DIS>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Translate.channel_Z.keyDistances.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Translate.channel_Z.keyDistances.get(j)) + "\n";
            }
            full += "</TRANS_Z_DIS>\n";
            
            
            //FOR ROTATION
            //FOR ALL X VALUES IN ROTATE
            full += "<ROT_X_VAL>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Rotate.channel_X.keyValue.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Rotate.channel_X.keyValue.get(j)) + "\n";
            }
            full += "</ROT_X_VAL>\n";
            
            full += "<ROT_X_DIS>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Rotate.channel_X.keyDistances.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Rotate.channel_X.keyDistances.get(j)) + "\n";
            }
            full += "</ROT_X_DIS>\n";
            
            //FOR ALL Y VALUES IN ROTATE
            full += "<ROT_Y_VAL>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Rotate.channel_Y.keyValue.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Rotate.channel_Y.keyValue.get(j)) + "\n";
            }
            full += "</ROT_Y_VAL>\n";
            
            full += "<ROT_Y_DIS>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Rotate.channel_Y.keyDistances.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Rotate.channel_Y.keyDistances.get(j)) + "\n";
            }
            full += "</ROT_Y_DIS>\n";
            
            //FOR ALL Z VALUES IN ROTATE
            full += "<ROT_Z_VAL>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Rotate.channel_Z.keyValue.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Rotate.channel_Z.keyValue.get(j)) + "\n";
            }
            full += "</ROT_Z_VAL>\n";
            
            full += "<ROT_Z_DIS>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Rotate.channel_Z.keyDistances.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Rotate.channel_Z.keyDistances.get(j)) + "\n";
            }
            full += "</ROT_Z_DIS>\n";
            
            
            
            
            //FOR Scaling
            //FOR ALL X VALUES IN SCALE
            full += "<SCALE_X_VAL>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Scale.channel_X.keyValue.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Scale.channel_X.keyValue.get(j)) + "\n";
            }
            full += "</SCALE_X_VAL>\n";
            
            full += "<SCALE_X_DIS>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Scale.channel_X.keyDistances.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Scale.channel_X.keyDistances.get(j)) + "\n";
            }
            full += "</SCALE_X_DIS>\n";
            
            //FOR ALL Y VALUES IN SCALE
            full += "<SCALE_Y_VAL>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Scale.channel_Y.keyValue.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Scale.channel_Y.keyValue.get(j)) + "\n";
            }
            full += "</SCALE_Y_VAL>\n";
            
            full += "<SCALE_Y_DIS>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Scale.channel_Y.keyDistances.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Scale.channel_Y.keyDistances.get(j)) + "\n";
            }
            full += "</SCALE_Y_DIS>\n";
            
            //FOR ALL Z VALUES IN SCALE
            full += "<SCALE_Z_VAL>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Scale.channel_Z.keyValue.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Scale.channel_Z.keyValue.get(j)) + "\n";
            }
            full += "</SCALE_Z_VAL>\n";
            
            full += "<SCALE_Z_DIS>\n";
            for(int j = 0; j < anim_deformers.get(i).channel_Scale.channel_Z.keyDistances.size(); j++)
            {
                full += Float.toString(anim_deformers.get(i).channel_Scale.channel_Z.keyDistances.get(j)) + "\n";
            }
            full += "</SCALE_Z_DIS>\n";
            
            
            full += "</JOINT>\n";   
        }
        
        
        Path logFile = Paths.get(path+name);            
        BufferedWriter writer = Files.newBufferedWriter(
                    logFile, 
                    StandardCharsets.UTF_8,
                    StandardOpenOption.CREATE);
        writer.write(full);
        writer.close();
             
        return true;
    }
    
    public static boolean write_posenode_as_text(ArrayList<PoseNode> poseNodes_model, String path,  String name) throws FileNotFoundException, IOException
    {        
        String full = "";
        int count = poseNodes_model.size();
        
        for(int i = 0; i < count; i++)
        {
            String deformer_name = "";
            if(poseNodes_model.get(i).nodeName.contains("Model::"))
            {                
                full += "<POSENODE>\nNAME: " +  poseNodes_model.get(i).nodeName.substring(7) + "\n";
            }
            else
                continue;
            
            full += "<MATRIX>\n";
            for(int x = 0; x < poseNodes_model.get(i).matrix.size(); x++)
            {
                full += Float.toString(poseNodes_model.get(i).matrix.get(x))+ "\n";
            }
            full += "</MATRIX>\n";                       
            full += "</POSENODE>\n";
        }
                
        Path logFile = Paths.get(path+name);            
        BufferedWriter writer = Files.newBufferedWriter(
                    logFile, 
                    StandardCharsets.UTF_8,
                    StandardOpenOption.CREATE);
        writer.write(full);
        writer.close();
            
        return true;
    }      
        
    public static ArrayList<Joint_Animation_data> anim_deformer_to_animation_data(ArrayList<Animated_Deformer> defs)
    {
        ArrayList<Joint_Animation_data> j_anim_data = new ArrayList<>();
        
        for(int i = 0; i < defs.size(); i++)
        {
            Joint_Animation_data data_single = new Joint_Animation_data();
            
            data_single.jointName = defs.get(i).DeformerName;
            data_single.translate_x = defs.get(i).channel_Translate.channel_X.keyValue;
            data_single.translate_x_key = defs.get(i).channel_Translate.channel_X.keyDistances;
            
            data_single.translate_y = defs.get(i).channel_Translate.channel_Y.keyValue;
            data_single.translate_y_key = defs.get(i).channel_Translate.channel_Y.keyDistances;
            
            data_single.translate_z = defs.get(i).channel_Translate.channel_Z.keyValue;
            data_single.translate_z_key = defs.get(i).channel_Translate.channel_Z.keyDistances;
            
            
            data_single.rotate_x = defs.get(i).channel_Rotate.channel_X.keyValue;
            data_single.rotate_x_key = defs.get(i).channel_Rotate.channel_X.keyDistances;
            
            data_single.rotate_y = defs.get(i).channel_Rotate.channel_Y.keyValue;
            data_single.rotate_y_key = defs.get(i).channel_Rotate.channel_Y.keyDistances;
            
            data_single.rotate_z = defs.get(i).channel_Rotate.channel_Z.keyValue;
            data_single.rotate_z_key = defs.get(i).channel_Rotate.channel_Z.keyDistances;
                        
            
            data_single.scale_x = defs.get(i).channel_Scale.channel_X.keyValue;
            data_single.scale_x_key = defs.get(i).channel_Scale.channel_X.keyDistances;
            
            data_single.scale_y = defs.get(i).channel_Scale.channel_Y.keyValue;
            data_single.scale_y_key = defs.get(i).channel_Scale.channel_Y.keyDistances;
            
            data_single.scale_z = defs.get(i).channel_Scale.channel_Z.keyValue;
            data_single.scale_z_key = defs.get(i).channel_Scale.channel_Z.keyDistances;
            
            
            j_anim_data.add(data_single);
        }
        
        return j_anim_data;        
    }
    
  
    //Seperates all the joints for one take
    public static ArrayList<Animated_Deformer> get_Animated_Deformers_string(ArrayList<String> lines)
    {                
        ArrayList<Animated_Deformer_string> take_perjointdata = new ArrayList<>();
        Animated_Deformer_string node = new Animated_Deformer_string();
        
        
        ArrayList<Integer> begins = new ArrayList<>();
        ArrayList<Integer> ends = new ArrayList<>();
        
        int brackets = 0;
                
        for(int i = 0; i < lines.size(); i++)
        {
            if(lines.get(i).trim().startsWith("Takes: "))
                continue;
            
            if(lines.get(i).trim().startsWith("Take: "))
                continue;    
            
            if(lines.get(i).contains("Model: \"") && lines.get(i).contains("{"))
            {                
                brackets++;
                begins.add(i);
            }
            
            else if(lines.get(i).contains("{") && !lines.get(i).contains("Model: \""))
            {
                brackets++;
            }
            
            if(lines.get(i).contains("}"))
            {
                brackets--;
                if(brackets == 0)
                {
                    ends.add(i);
                }
            }            
        }
        
        for(int i = 0; i < begins.size(); i++)
        {
            node = new Animated_Deformer_string();
            for(int j = begins.get(i); j < ends.get(i); j++)
            {
                node.lines.add(lines.get(j));
            }
            
            take_perjointdata.add(node);
        }        
        
        ArrayList<Animated_Deformer> animated_deformers = new ArrayList<>();
                        
        for(Animated_Deformer_string deformer : take_perjointdata)
        {
            try
            {
                animated_deformers.add(get_Animated_Deformers_string(deformer)); // Now go into each main channel
            }
            catch(Exception e)
            {
                System.out.println("failed");              
            }
        }
        
        System.out.println("After loop");
        
        return animated_deformers;
    }
    
    //Get the 3 main channels in string form and return 3 channel_string
    public static Animated_Deformer get_Animated_Deformers_string(Animated_Deformer_string def_Str)
    {
        //   Model: "Model::pasted__pelvis" {
        int len = def_Str.lines.get(0).trim().length();
        String deformer_name = def_Str.lines.get(0).trim().substring(8,len-3);
        
        Channel_String channel_T = new Channel_String();
        SubChannel_String sub_t_x = new SubChannel_String();
        SubChannel_String sub_t_y = new SubChannel_String();
        SubChannel_String sub_t_z = new SubChannel_String();
        
        Channel_String channel_R = new Channel_String();
        SubChannel_String sub_r_x = new SubChannel_String();
        SubChannel_String sub_r_y = new SubChannel_String();
        SubChannel_String sub_r_z = new SubChannel_String();
        
        Channel_String channel_S = new Channel_String();
        SubChannel_String sub_s_x = new SubChannel_String();
        SubChannel_String sub_s_y = new SubChannel_String();
        SubChannel_String sub_s_z = new SubChannel_String();
        
        int bracketCounter = 0;
        
        //Extract MainChannel Tripplet        
         //<editor-fold>      
        for(String line : def_Str.lines)
        {
            if(line.trim().startsWith("Channel: \"T\" {"))
            {
                bracketCounter++;              
                channel_T.lines.add(line);
                continue;              
            }
            if(line.trim().contains("{") && bracketCounter > 0)
            {                
                bracketCounter++;               
            }
            
            if(line.trim().contains("}") && bracketCounter > 0)
            {
                bracketCounter--;
            }
            
            if(bracketCounter > 0)
            {                
                channel_T.lines.add(line);           
            }
        }
        
        for(String line : def_Str.lines)
        {
            if(line.trim().startsWith("Channel: \"R\" {"))
            {
                bracketCounter++;              
                channel_R.lines.add(line);
                continue;
            }
            if(line.trim().contains("{") && bracketCounter > 0)
            {
                bracketCounter++;
            }
            
            if(line.trim().contains("}") && bracketCounter > 0)
            {
                bracketCounter--;               
            }
            
            if(bracketCounter > 0)
            {                
                channel_R.lines.add(line);           
            }
        }
        
        for(String line : def_Str.lines)
        {
            if(line.trim().startsWith("Channel: \"S\" {"))
            {
                bracketCounter++;              
                channel_S.lines.add(line);
                continue;
            }
            if(line.trim().contains("{") && bracketCounter > 0)
            {
                bracketCounter++;
            }
            
            if(line.trim().contains("}") && bracketCounter > 0)
            {
                bracketCounter--;
            }
            
            if(bracketCounter > 0)
            {
                channel_S.lines.add(line);
            }
        }
         //</editor-fold>
        
         //now start dissecting each subchannel for x,y,z in Translate         
         //<editor-fold>        
         bracketCounter = 0;
         int i = 0;
         for(; i < channel_T.lines.size(); i++)
         {
                if(channel_T.lines.get(i).trim().startsWith("Channel: \"T\" {"))
                {                    
                    continue;
                }             
                if(channel_T.lines.get(i).trim().startsWith("Channel: \"X\" {"))
                {
                    bracketCounter++;                    
                    sub_t_x.lines.add(channel_T.lines.get(i));
                    continue;
                }
                if(channel_T.lines.get(i).trim().contains("{"))
                {                
                    bracketCounter++;               
                }

                if(channel_T.lines.get(i).trim().contains("}"))
                {
                    bracketCounter--;
                }         
                
                if(channel_T.lines.get(i).trim().contains("Channel: \"Y\" {"))
                {                
                    break;
                }
                
                if(bracketCounter > 0)
                {
                    sub_t_x.lines.add(channel_T.lines.get(i));
                }
         }
                 
         for(; i < channel_T.lines.size(); i++)
         {                         
                if(channel_T.lines.get(i).trim().startsWith("Channel: \"Y\" {"))
                {
                    bracketCounter++;                    
                    sub_t_y.lines.add(channel_T.lines.get(i));
                    continue;
                }
                if(channel_T.lines.get(i).trim().contains("{"))
                {                
                    bracketCounter++;               
                }

                if(channel_T.lines.get(i).trim().contains("}"))
                {
                    bracketCounter--;
                }         
                
                if(channel_T.lines.get(i).trim().contains("Channel: \"Z\" {"))
                {                
                    break;
                }
                
                if(bracketCounter > 0)
                {
                    sub_t_y.lines.add(channel_T.lines.get(i));
                }
         }
         
         bracketCounter = 0;
         for(; i < channel_T.lines.size(); i++)
         {                         
                if(channel_T.lines.get(i).trim().startsWith("Channel: \"Z\" {"))
                {
                    bracketCounter++;                    
                    sub_t_z.lines.add(channel_T.lines.get(i));
                    continue;
                }
                if(channel_T.lines.get(i).trim().contains("{"))
                {                
                    bracketCounter++;               
                }

                if(channel_T.lines.get(i).trim().contains("}"))
                {
                    bracketCounter--;
                }         
                
                if(channel_T.lines.get(i).trim().contains("Color: "))
                {                
                    break;
                }
                
                if(bracketCounter > 0)
                {
                    sub_t_z.lines.add(channel_T.lines.get(i));
                }
         }       
         //</editor-fold>    
                  
         //now start dissecting each subchannel for x,y,z in Rotate
         //<editor-fold>        
         bracketCounter = 0;
         i = 0;
         for(; i < channel_R.lines.size(); i++)
         {
                if(channel_R.lines.get(i).trim().startsWith("Channel: \"R\" {"))
                {                    
                    continue;
                }             
                if(channel_R.lines.get(i).trim().startsWith("Channel: \"X\" {"))
                {
                    bracketCounter++;                    
                    sub_r_x.lines.add(channel_R.lines.get(i));
                    continue;
                }
                if(channel_R.lines.get(i).trim().contains("{"))
                {                
                    bracketCounter++;               
                }

                if(channel_R.lines.get(i).trim().contains("}"))
                {
                    bracketCounter--;
                }         
                
                if(channel_R.lines.get(i).trim().contains("Channel: \"Y\" {"))
                {                
                    break;
                }
                
                if(bracketCounter > 0)
                {
                    sub_r_x.lines.add(channel_R.lines.get(i));
                }
         }
                 
         for(; i < channel_R.lines.size(); i++)
         {                         
                if(channel_R.lines.get(i).trim().startsWith("Channel: \"Y\" {"))
                {
                    bracketCounter++;                    
                    sub_r_y.lines.add(channel_R.lines.get(i));
                    continue;
                }
                if(channel_R.lines.get(i).trim().contains("{"))
                {                
                    bracketCounter++;               
                }

                if(channel_R.lines.get(i).trim().contains("}"))
                {
                    bracketCounter--;
                }         
                
                if(channel_R.lines.get(i).trim().contains("Channel: \"Z\" {"))
                {                
                    break;
                }
                
                if(bracketCounter > 0)
                {
                    sub_r_y.lines.add(channel_R.lines.get(i));
                }
         }
         
         bracketCounter = 0;
         for(; i < channel_R.lines.size(); i++)
         {                         
                if(channel_R.lines.get(i).trim().startsWith("Channel: \"Z\" {"))
                {
                    bracketCounter++;                    
                    sub_r_z.lines.add(channel_R.lines.get(i));
                    continue;
                }
                if(channel_R.lines.get(i).trim().contains("{"))
                {                
                    bracketCounter++;               
                }

                if(channel_R.lines.get(i).trim().contains("}"))
                {
                    bracketCounter--;
                }         
                
                if(channel_R.lines.get(i).trim().contains("Color: "))
                {                
                    break;
                }
                
                if(bracketCounter > 0)
                {
                    sub_r_z.lines.add(channel_R.lines.get(i));
                }
         }       
         //</editor-fold>  
         
         //now start dissecting each subchannel for x,y,z in Translate         
         //<editor-fold>        
         bracketCounter = 0;
         i = 0;
         for(; i < channel_S.lines.size(); i++)
         {
                if(channel_S.lines.get(i).trim().startsWith("Channel: \"T\" {"))
                {                    
                    continue;
                }             
                if(channel_S.lines.get(i).trim().startsWith("Channel: \"X\" {"))
                {
                    bracketCounter++;                    
                    sub_s_x.lines.add(channel_S.lines.get(i));
                    continue;
                }
                if(channel_S.lines.get(i).trim().contains("{"))
                {                
                    bracketCounter++;               
                }

                if(channel_S.lines.get(i).trim().contains("}"))
                {
                    bracketCounter--;
                }         
                
                if(channel_S.lines.get(i).trim().contains("Channel: \"Y\" {"))
                {                
                    break;
                }
                
                if(bracketCounter > 0)
                {
                    sub_s_x.lines.add(channel_S.lines.get(i));
                }
         }
                 
         for(; i < channel_S.lines.size(); i++)
         {                         
                if(channel_S.lines.get(i).trim().startsWith("Channel: \"Y\" {"))
                {
                    bracketCounter++;                    
                    sub_s_y.lines.add(channel_S.lines.get(i));
                    continue;
                }
                if(channel_S.lines.get(i).trim().contains("{"))
                {                
                    bracketCounter++;               
                }

                if(channel_S.lines.get(i).trim().contains("}"))
                {
                    bracketCounter--;
                }         
                
                if(channel_S.lines.get(i).trim().contains("Channel: \"Z\" {"))
                {                
                    break;
                }
                
                if(bracketCounter > 0)
                {
                    sub_s_y.lines.add(channel_S.lines.get(i));
                }
         }
         
         bracketCounter = 0;
         for(; i < channel_S.lines.size(); i++)
         {                         
                if(channel_S.lines.get(i).trim().startsWith("Channel: \"Z\" {"))
                {
                    bracketCounter++;                    
                    sub_s_z.lines.add(channel_S.lines.get(i));
                    continue;
                }
                if(channel_S.lines.get(i).trim().contains("{"))
                {                
                    bracketCounter++;               
                }

                if(channel_S.lines.get(i).trim().contains("}"))
                {
                    bracketCounter--;
                }         
                
                if(channel_S.lines.get(i).trim().contains("Color: "))
                {                
                    break;
                }
                
                if(bracketCounter > 0)
                {
                    sub_s_z.lines.add(channel_S.lines.get(i));
                }
         }       
         //</editor-fold>  
                       
         //Now We will pick out the relevant key data
         
         KeyVal_Box box_t_x = extractKeys(sub_t_x.lines);
         KeyVal_Box box_t_y = extractKeys(sub_t_y.lines);
         KeyVal_Box box_t_z = extractKeys(sub_t_z.lines);
         
         KeyVal_Box box_r_x = extractKeys(sub_r_x.lines);
         KeyVal_Box box_r_y = extractKeys(sub_r_y.lines);
         KeyVal_Box box_r_z = extractKeys(sub_r_z.lines);
         
         KeyVal_Box box_s_x = extractKeys(sub_s_x.lines);
         KeyVal_Box box_s_y = extractKeys(sub_s_y.lines);
         KeyVal_Box box_s_z = extractKeys(sub_s_z.lines);
                        
        Channel channel_T_bin = new Channel();
        SubChannel sub_t_x_bin = new SubChannel();
        SubChannel sub_t_y_bin = new SubChannel();
        SubChannel sub_t_z_bin = new SubChannel();
        
        Channel channel_R_bin = new Channel();
        SubChannel sub_r_x_bin = new SubChannel();
        SubChannel sub_r_y_bin = new SubChannel();
        SubChannel sub_r_z_bin = new SubChannel();
        
        Channel channel_S_bin = new Channel();
        SubChannel sub_s_x_bin = new SubChannel();
        SubChannel sub_s_y_bin = new SubChannel();
        SubChannel sub_s_z_bin = new SubChannel();
        
        sub_t_x_bin.keyDistances = box_t_x.keys;
        sub_t_x_bin.keyValue = box_t_x.values;
        sub_t_y_bin.keyDistances = box_t_y.keys;
        sub_t_y_bin.keyValue = box_t_y.values;
        sub_t_z_bin.keyDistances = box_t_z.keys;
        sub_t_z_bin.keyValue = box_t_z.values;
        
        sub_r_x_bin.keyDistances = box_r_x.keys;
        sub_r_x_bin.keyValue = box_r_x.values;
        sub_r_y_bin.keyDistances = box_r_y.keys;
        sub_r_y_bin.keyValue = box_r_y.values;
        sub_r_z_bin.keyDistances = box_r_z.keys;
        sub_r_z_bin.keyValue = box_r_z.values;
        
        sub_s_x_bin.keyDistances = box_s_x.keys;
        sub_s_x_bin.keyValue = box_s_x.values;
        sub_s_y_bin.keyDistances = box_s_y.keys;
        sub_s_y_bin.keyValue = box_s_y.values;
        sub_s_z_bin.keyDistances = box_s_z.keys;
        sub_s_z_bin.keyValue = box_s_z.values;
        
        Channel T_bin = new Channel();
        Channel R_bin = new Channel();
        Channel S_bin = new Channel();
        
        T_bin.channel_X = sub_t_x_bin;
        T_bin.channel_Y = sub_t_y_bin;
        T_bin.channel_Z = sub_t_z_bin;
        
        R_bin.channel_X = sub_r_x_bin;
        R_bin.channel_Y = sub_r_y_bin;
        R_bin.channel_Z = sub_r_z_bin;
        
        S_bin.channel_X = sub_s_x_bin;
        S_bin.channel_Y = sub_s_y_bin;
        S_bin.channel_Z = sub_s_z_bin;
        
        Animated_Deformer deformer = new Animated_Deformer();
        
        deformer.channel_Translate = T_bin;
        deformer.channel_Rotate = R_bin;
        deformer.channel_Scale = S_bin;
        deformer.DeformerName = deformer_name;
        
        return deformer;
    }
            
    //Make final deformers
    public static ArrayList<Deformer> make_deformers(ArrayList<DeformerNodes_String> defs)
    {
        ArrayList<Deformer> deformers = new ArrayList<Deformer>();
        
        for(DeformerNodes_String def : defs)
        {
            System.out.println(def.lines);
            Deformer deformer = new Deformer();
            ArrayList<String> index_lines = new ArrayList<>();
            ArrayList<String> weight_lines = new ArrayList<>();
            String indices_line = "";
            String weight_line = "";
            Boolean readIndexes = false;
            Boolean readWeights = false;
            Boolean read_trans_lines = false;
            Boolean read_trans_lines_link = false;
            String transformLines = "";
            String transformLinesLink = "";
            
            for(String line : def.lines)
            {
                // Deformer: "SubDeformer::Cluster_pelvis", "Cluster" {
                if(line.trim().startsWith("Deformer: "))
                {
                    deformer.DeformerName = line.substring(10).trim().split(" ")[1];
                    deformer.DeformerName = deformer.DeformerName.substring(1, deformer.DeformerName.length()-2);
                }
                
                if(line.trim().startsWith("Transform: "))
                {
                    read_trans_lines = true;
                }
                
                if(line.trim().startsWith("TransformLink: "))
                { 
                    read_trans_lines = false;  
                    read_trans_lines_link = true;
                }                
                
                if(line.trim().startsWith("}"))
                { 
                    read_trans_lines = false;                    
                }
                
                if(read_trans_lines)
                {
                    transformLines += line;                    
                }
                
                if(read_trans_lines_link)
                {
                    transformLinesLink += line;                    
                }
                
                if(line.trim().startsWith("Indexes: "))
                {
                    readIndexes = true;
                }                               
                
                if(line.trim().startsWith("Weights: "))
                {
                    readIndexes = false;
                    readWeights = true;              
                }
                
                if(line.trim().startsWith("Transform:"))
                    readWeights = false;
                
                if(readIndexes)
                {
                    indices_line += line.trim();
                }
                
                if(readWeights)
                {
                    weight_line += line.trim();
                }
            }
            
            String[] w, i;
            w = new String[0];            
            i = new String[0];
            
            String wline, iline;
            wline = weight_line;
            iline = indices_line;
            
            Boolean noweight = false;
            Boolean noindices = false;
            
            if(weight_line.length() > 0)
            {
                w = weight_line.substring(9).split(",");
            }
            if(indices_line.length() > 0)
            {  
                i = indices_line.substring(9).split(",");
            }
            
            
            for(String l : w)
                deformer.weights.add(Float.parseFloat(l));
            
            if(w.length < 1)
                deformer.weights.add(0.0f);
            
            for(String l : i)
                deformer.indexes.add(Integer.parseInt(l));
                                 
            if(i.length < 1)
                deformer.indexes.add(0);            
            
            
            
            String[] translines_split = transformLines.replaceAll("\\s+","").substring(10).split(",");
            for(String t : translines_split)
            {
                deformer.Transform.add(Float.parseFloat(t));
            }
            
            String[] translineslink_split = transformLinesLink.replaceAll("\\s+","").substring(14).split(",");
            for(String t : translineslink_split)
            {               
                deformer.TransformLink.add(Float.parseFloat(t));
            }
            
            deformers.add(deformer);
        }        
        return deformers;
    }
    
    //Get deformerNodes as String
    public static ArrayList<DeformerNodes_String> get_deformer_nodes(ArrayList<String> lines)
    {
        ArrayList<DeformerNodes_String> def_nodes_string = new ArrayList<>();
        DeformerNodes_String node = new DeformerNodes_String();
        
        int bracketCounter = 0;        
        for(String line : lines)
        {            
            if(line.trim().startsWith("Deformer: \"SubDeformer"))
            {
                if(node.lines.size() > 1)
                    def_nodes_string.add(node);                 
                node = new DeformerNodes_String();                
                bracketCounter++;
            }
            if(line.trim().startsWith("}"))
            {
                bracketCounter--;
            }
            node.lines.add(line);
        }
        return def_nodes_string;        
    }    
    
    public static KeyVal_Box extractKeys(ArrayList<String> lines) //Return 2 key lists for float/long as val/keystep
    {
        //SUBTRACT FIRST KEY FROM THE REST 1924423250, since this is the start of animation
        //Find constant key dividor (for fbx) and divide keys by that.
        //Dividor is 1912395604   , it was found by  doing  (last_key_time - starting_offset)/ 160 (its know that the animation has 160 frames 
        
        KeyVal_Box box = new KeyVal_Box();
        String resultString = "";
        boolean read = false;
        for(String line : lines)
        {
            if(line.trim().startsWith("Key: "))
            {
                read = true;
            }
            if(line.trim().startsWith("Color: "))
            {
                read = false;
            }
            if(read){                 
                resultString+= line.trim();
            }
        }
       String[] keyStrings =  resultString.trim().substring(4).split(",");
       
       ArrayList<Float> vals = new ArrayList<>();
       ArrayList<Long> keys = new ArrayList<>();
       
       try
       {
            for(int i = 0; i < keyStrings.length; i+=7)
            {
                 vals.add(Float.parseFloat(keyStrings[i+1].trim()));
                 keys.add(( (Long.parseLong(keyStrings[i+0].trim())-1924423250)/1912395604)+1);
            }       
       }
       catch(Exception e)
       {
           System.out.println(e.getMessage());
       }
       
       box.keys = keys;
       box.values = vals;
       return box;
    }
    
    //Create the connections for the mesh/deformers
    public static ArrayList<Connection> createConnections(ArrayList<String> cons)
    {
        ArrayList<Connection> connections = new ArrayList<>();        
        for(String line  : cons)
        {            
            if(line.trim().startsWith("Connect: "))
            {
                if(line.contains("Deformer::") ||line.contains("SubDeformer::"))
                    continue;
                                
                Connection con = new Connection();
                String thisline = line.trim().substring(8);
                String[] splited = thisline.split(",");
                splited[0] = splited[0].trim().substring(1,splited[0].length()-2);
                splited[1] = splited[1].trim().substring(1,splited[1].length()-2);
                splited[2] = splited[2].trim().substring(1,splited[2].length()-2);

                con.child = splited[2];
                con.parent = splited[1];
                
                connections.add(con);
            }
        }        
        return connections;
    }
    
    //Extract texture info
    public static Shader getTextureInfo(ArrayList<String> strings)
    {
        Shader s = new Shader();
        for(String line : strings)
        {
            if(line.trim().startsWith("RelativeFilename: "))
            {
                String theline = line.trim().substring(19);
                s.relTextureName = theline.substring(0,theline.length()-1);
                break;
            }            
        }
        return s;
    }
    
    //Create posenodes from Strings
    public static ArrayList<PoseNode> createPoseNodes(ArrayList<PoseNode_string> str_nodes)
    {
        ArrayList<PoseNode> nodes = new ArrayList<>();
               
        for(PoseNode_string nodestr : str_nodes)
        {
            PoseNode node = new PoseNode();
                        
            for(String line : nodestr.poseNodeLines)
            {
                if(line.startsWith("Node: "))
                {
                    String nodeline = line.trim().substring(7, line.length()-1);                    
                    node.nodeName = nodeline;
                    
                }                
            }
            
            String matrixLine = "";
            boolean readmatrix = false;
            
            for(String line : nodestr.poseNodeLines)
            {                
                if(line.startsWith("Matrix: "))
                {
                    readmatrix = true;
                    String nodeline = line.trim().substring(8);
                    
                    matrixLine += nodeline;    
                    continue;
                }
                if(readmatrix)
                    matrixLine += line;
                
                if(line.contains("}"))
                    break;                
            }
            
            String[] matrixl = matrixLine.split(",");
            ArrayList<Float> floats = new ArrayList();
            for(int i =0; i < matrixl.length; i++)
            {
                floats.add( Float.parseFloat(matrixl[i]));
            }
            node.matrix = floats;
                        
            nodes.add(node);
        }        
        return nodes;            
    }  
    
    //make the mesh
    public static Mesh_data_static createMeshDataStatic(Mesh_Lines_Packed modelmeshpack)throws ArithmeticException
    {   
        
        ArrayList<Float> point_list = null;
        ArrayList<Integer> point_index_list=null;
        ArrayList<Float> uv_list =null;
        ArrayList<Integer> uv_indices=null;
        ArrayList<Float> normals_floats=null;
        
        try{
            point_list = (ArrayList<Float>)extractArray(modelmeshpack.verticeLines,false);
            System.out.println("point succesful");
        }
        catch(Exception e)
        { System.out.println("Failed to parse Points");}
        
        try{
            point_index_list = (ArrayList<Integer>)extractArray(modelmeshpack.vindex_lines, true);
            System.out.println("point index succesful");
        }
        catch(Exception e){ System.out.println("Failed to parse point indices");}
        
        try{
             uv_list = (ArrayList<Float>)extractArray(modelmeshpack.uv_lines,false);
            System.out.println("uv succesful");
        }
        catch(Exception e){ System.out.println("Failed to parse uvs");}
        
        try{
            uv_indices = (ArrayList<Integer>)extractArray(modelmeshpack.uv_index_lines, true);
            System.out.println("uv index succesful");
        }
        catch(Exception e){ System.out.println("Failed to parse uv index");}
        
        try{
            normals_floats = (ArrayList<Float>)extractArray(modelmeshpack.normals_lines, false);
            System.out.println("normals suc");
        }
        catch(Exception e){ System.out.println("Failed to parse normals");
        }
                


        Mesh_data_static mesh = new Mesh_data_static();
        mesh.normals = normals_floats;
        mesh.p_index = point_index_list;
        mesh.points = point_list;
        mesh.uv = uv_list;
        mesh.uv_index = uv_indices;
        mesh.meshName = modelmeshpack.mesh_name;
        return mesh;
    }
    
    //Get the arrays from a string of numbers
    public static Object extractArray(ArrayList<String> lines, boolean indexMode)
    {
        String resline = "";
        for(String line : lines)
        {
            resline += line;
        }
        int len = resline.trim().length();
        String[] floats = resline.split(",");
        
        ArrayList<Float> float_list = new ArrayList<>();
        ArrayList<Integer> integer_list = new ArrayList<>();
        
        resline = null;
        for(String f : floats)
        {
            if(!indexMode)
            {
                float thef = Float.parseFloat(f);                
                float_list.add(thef);  
            }            
            else
            {
                Integer thei = Integer.parseInt(f);
                
                if(thei < 0)
                    integer_list.add(thei*(-1)-1);
                if(thei >= 0)
                    integer_list.add(thei);  
            }
        }
        
        if(indexMode)
            return integer_list;
        else
            return float_list;
    }    
    
    //get Deformers in this code
    //<editor-fold>
    public static ArrayList<String> fbx_get_takes(ArrayList<String> wholelines, long time)
    {
        ArrayList<String> takelines = new ArrayList<>();
        int bracket_counter = 0;
        boolean readTakes = false;
        
        for(int i = 0; i < wholelines.size()-1; i++)
        {        
            if(wholelines.get(i).contains("Takes:  {"))
            {                
                bracket_counter++;
                readTakes = true;
            }            
            else if(wholelines.get(i).contains("}") && readTakes == true)
            {
                bracket_counter--;
            }
            else if (wholelines.get(i).contains("{") && readTakes == true)
            {
                bracket_counter++;
            }
            
            if(bracket_counter > 0 && readTakes)
            {
                takelines.add(wholelines.get(i));
            }
            if(bracket_counter < 1 && readTakes)
            {
                readTakes = false;
            }
        }        
        return takelines;
    }
        
    //get Deformers in this code
    //<editor-fold>
    public static ArrayList<String> fbx_get_texture(ArrayList<String> wholelines, long time)
    {
        ArrayList<String> textureLines = new ArrayList<>();
        int bracket_counter = 0;
        boolean textureOpen = false;
        
        for(int i = 0; i < wholelines.size()-1; i++)
        {        
            if( (wholelines.get(i).contains("Texture: ")  && wholelines.get(i).contains("Texture::") && wholelines.get(i).contains("\"TextureVideoClip\"")) && wholelines.get(i).contains("{"))
            {
                String line = wholelines.get(i);
                bracket_counter++;
                textureOpen = true;
            }            
            else if(wholelines.get(i).contains("}") && textureOpen == true)
            {
                bracket_counter--;
            }
            else if (wholelines.get(i).contains("{") && textureOpen == true)
            {
                bracket_counter++;
            }
            
            if(bracket_counter > 0 && textureOpen)
            {
                textureLines.add(wholelines.get(i));
            }
            if(bracket_counter < 1 && textureOpen)
            {
                textureOpen = false;
            }
        }        
        return textureLines;
    }
    
    //get Deformers in this code
    //<editor-fold>
    public static ArrayList<String> fbx_get_connections(ArrayList<String> wholelines, long time)
    {
        ArrayList<String> connections = new ArrayList<>();
        int bracket_counter = 0;
        boolean connectionOpen = false;
        
        for(int i = 0; i < wholelines.size()-1; i++)
        {        
            if(wholelines.get(i).contains("Connections:  {"))
            {
                String line = wholelines.get(i);
                bracket_counter++;
                connectionOpen = true;
            }            
            else if(wholelines.get(i).contains("}") && connectionOpen == true)
            {
                bracket_counter--;
            }
            else if (wholelines.get(i).contains("{") && connectionOpen == true)
            {
                bracket_counter++;
            }
            
            if(bracket_counter > 0 && connectionOpen)
            {
                connections.add(wholelines.get(i));
            }
            if(bracket_counter < 1 && connectionOpen)
            {
                connectionOpen = false;
            }
        }
                
        return connections;
    }
    
    //get Deformers in this code
    //<editor-fold>
    public static ArrayList<String> fbx_get_deformers(ArrayList<String> wholelines, long time)
    {
        ArrayList<String> defor_lines = new ArrayList<>();
        int bracket_counter = 0;
        boolean deformerOpen = false;
        
        for(int i = 0; i < wholelines.size()-1; i++)
        {        
            if( (wholelines.get(i).contains("Deformer: ")  && wholelines.get(i).contains("SubDeformer::") && wholelines.get(i).contains("\"Cluster\"")) && wholelines.get(i).contains("{"))
            {
                String line = wholelines.get(i);
                bracket_counter++;
                deformerOpen = true;
            }            
            else if(wholelines.get(i).contains("}") && deformerOpen == true)
            {
                bracket_counter--;
            }
            else if (wholelines.get(i).contains("{") && deformerOpen == true)
            {
                bracket_counter++;
            }
            
            if(bracket_counter > 0 && deformerOpen)
            {
                defor_lines.add(wholelines.get(i));
            }
            if(bracket_counter < 1 && deformerOpen)
            {
                deformerOpen = false;
            }
        }        
        return defor_lines;
    }
    
    //get posenodes but as string lists still
    //<editor-fold>
    public static ArrayList<PoseNode_string> fbx_get_bindPoselines(ArrayList<String> wholelines, long time)
    {       
        ArrayList<String> posenodes_lines = new ArrayList<>();
        boolean start_reading = false;       
        
        for(int i = 0; i < wholelines.size()-1; i++)
        {     
            if(wholelines.get(i).trim().startsWith("PoseNode:  {"))
            {
                start_reading = true;
                
            }
            if(start_reading)
            {
                posenodes_lines.add(wholelines.get(i).trim());
            }
            
            if(wholelines.get(i).contains("}") && wholelines.get(i+1).contains("}"))
            {
                start_reading = false;                
            }                        
        }
        
        ArrayList<PoseNode_string> poseNode_list = new ArrayList<>();        
        PoseNode_string pose = new PoseNode_string();
        boolean startReading = false;
                
        for(int i = 0; i < posenodes_lines.size(); i++)
        {            
            if(posenodes_lines.get(i).startsWith("PoseNode: "))
                pose = new PoseNode_string();  
            
            while(true)
            {
                pose.poseNodeLines.add(posenodes_lines.get(i));
                i++;
                if(posenodes_lines.get(i).startsWith("}"))
                {
                    break;
                }      
            }
            
            poseNode_list.add(pose);
        }
        
        System.out.println(posenodes_lines.size());
        return poseNode_list;
    }
    
    //get modelmesh related data here
    //<editor-fold>
    public static Mesh_Lines_Packed fbx_createModelMeshLines(ArrayList<String> meshlines, long time)
    {   
        boolean readVertices = false;
        boolean readVertexIndex = false;
        boolean readNormals = false;
        boolean read_uv = false;
        boolean read_uv_index = false;
        
        int started, ended;
        
        ArrayList<String> verticeLines = new ArrayList<>();
        ArrayList<String> vindex_lines = new ArrayList<>();
        ArrayList<String> normals_lines = new ArrayList<>();
        ArrayList<String> uv_lines = new ArrayList<>();
        ArrayList<String> uv_index_lines = new ArrayList<>();
        
        if(meshlines.size() < 1)
        {
            System.out.println("No mesh data");
            return null;
        }
        String name = meshlines.get(0).substring(8);
        name = name.substring(0,name.indexOf("\""));
        
        for(String line : meshlines)
        {
            if(line.startsWith("Vertices: "))
            {                
                readVertices = true;
                
            }
            
            if(line.contains("PolygonVertexIndex"))
            {                
                readVertices = false;
            }
            
            if(readVertices)
            {
                if(line.contains("Vertices: "))
                    verticeLines.add(line.substring(10));
                else
                    verticeLines.add(line);
            }    
            
            
            //Do the same with VertexINDEX
            if(line.contains("PolygonVertexIndex: "))
            {                
                readVertexIndex = true;
            }
            
            if(line.contains("Edges"))
            {                
                readVertexIndex = false;
            }
            
            if(readVertexIndex)
            {
                if(line.contains("PolygonVertexIndex: "))
                    vindex_lines.add(line.substring(20));
                else
                    vindex_lines.add(line);
            }        
                        
            //Do the same with VertexINDEX
            if(line.contains("Normals: "))
            {                
                readNormals = true;
            }
            
            if(line.contains("}"))
            {                
                readNormals = false;
            }
            
            if(readNormals)
            {
                if(line.contains("Normals: "))
                    normals_lines.add(line.substring(9));
                else
                    normals_lines.add(line);
            }       
            
            //Do the same with VertexINDEX
            if(line.startsWith("UV: "))
            {   
                if(uv_lines.size() < 1)
                {
                    read_uv = true;
                }
            }
            
            if(line.contains("UVIndex: "))
            {                
                read_uv = false;
            }
            
            if(read_uv)
            {
                if(line.startsWith("UV: "))
                    uv_lines.add(line.substring(4));
                else
                    uv_lines.add(line);
            }       
            
            //Do the same with VertexINDEX
            if(line.contains("UVIndex: "))
            {                
                if(uv_index_lines.size() < 1)
                {
                     read_uv_index = true;
                }
            }
            
            if(line.contains("}"))
            {                
                read_uv_index = false;
            }
            
            if(read_uv_index)
            {
                if(line.contains("UVIndex: "))
                    uv_index_lines.add(line.substring(9));
                else
                    uv_index_lines.add(line);
            }                   
        }        
        
        long elapsedTime = (System.nanoTime() - time) / 1000000;
        System.out.println(elapsedTime);
        Mesh_Lines_Packed pack = new Mesh_Lines_Packed();
        
        pack.verticeLines = verticeLines;
        pack.vindex_lines = vindex_lines;
        pack.normals_lines = normals_lines;
        pack.uv_lines = uv_lines;
        pack.uv_index_lines = uv_index_lines;
        pack.mesh_name = name;
        return pack;
    }
    //</editor-fold>
}
