/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parsernew;

import java.util.List;

/**
 *
 * @author Conex
 */
public class OBJ_Model
{
    public Vertex_Data vertex_data;
    public List<Group> group_data;    
    public String material_path;
    public List<Material> materials;
    
    public OBJ_Model()
    {     
    }
    
    public OBJ_Model(Vertex_Data vertex_data,List<Group> group_data,String material_path, List<Material> materials)
    {
        this.vertex_data = vertex_data;
        this.group_data = group_data; 
        this.material_path = material_path;
        this.materials = materials;
    }    
}