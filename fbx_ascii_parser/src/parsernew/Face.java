package parsernew;

import java.util.ArrayList;

public class Face
{
    public String appliedShader;
    ArrayList<Integer> point_indices;
    ArrayList<Integer> uv_indices;    
    ArrayList<Integer> vn_indices;    
        
    public Face()
    {
        point_indices = new ArrayList<>();
        uv_indices = new ArrayList<>();
        vn_indices = new ArrayList<>();
    }
}
