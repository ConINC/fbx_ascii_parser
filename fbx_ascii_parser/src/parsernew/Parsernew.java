package parsernew;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javax.swing.JFileChooser;

public class Parsernew extends Application {
    List<File> files;
    String listString = "";
    TextArea openFiles;
    @Override
    public void start(Stage primaryStage) {
        
        ArrayList<String> filesForConvert = new ArrayList<String>();
        
        int width = 600, height = 300;
        StackPane root = new StackPane();
        
        ProgressBar bar = new ProgressBar();        
        bar.maxWidth(580);
        bar.setMinWidth(580);
        bar.setTranslateY(-130);
        
        openFiles = new TextArea();
        openFiles.setText("Noone Selected");
        openFiles.setText("Add Files");
        openFiles.setFont(new Font(16));     
        openFiles.setMaxSize(420, 160);
        openFiles.setTranslateX(-80);
        openFiles.setTranslateY(+60);
        
        
        Button openFile = new Button();
        openFile.setTranslateX(width/2-80);
        openFile.setTranslateY(-height/2+150);
        openFile.setText("Add Files");
        openFile.setFont(new Font(16));
        openFile.setMaxSize(140, 30);        
                
        
        Button clear_files_list = new Button();
        clear_files_list.setTranslateX(width/2-80);
        clear_files_list.setTranslateY(-height/2+200);
        clear_files_list.setText("Clear Files");
        clear_files_list.setFont(new Font(16));
        clear_files_list.setMaxSize(140, 30);    
        
        
        Button convertFiles = new Button();
        convertFiles.setTranslateX(width/2-80);
        convertFiles.setTranslateY(120);
        convertFiles.setText("Convert");
        convertFiles.setMaxSize(140, 30);
        convertFiles.setFont(new Font(16));

        Button pickDirectory = new Button();
        pickDirectory.setTranslateX(width/2-80);
        pickDirectory.setTranslateY(-60);
        pickDirectory.setText("Pick Directory");
        pickDirectory.setMaxSize(140, 30);
        pickDirectory.setFont(new Font(16));

        TextField dirField = new TextField();
        dirField.setTranslateX(-80);
        dirField.setTranslateY(+30);        
        dirField.setText("Folder to save files to");
        dirField.setFont(new Font(16));     
        dirField.setMaxSize(420, 20);
        dirField.setTranslateX(-80);
        dirField.setTranslateY(-60);
       
        Label isobj_label = new Label();
        isobj_label.setTranslateX(width/2-135);
        isobj_label.setTranslateY(40);
        
        Label isFbx_label = new Label();
        isFbx_label.setTranslateX(width/2-135);
        isFbx_label.setTranslateY(40);
        
        Label hasAnimation_label = new Label();
        hasAnimation_label.setTranslateX(width/2-135);
        hasAnimation_label.setTranslateY(40);
        hasAnimation_label.setText("THIS IS A LONG LABEL!!");
        hasAnimation_label.setMaxWidth(100);
        
        openFile.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                FileChooser chooser = new FileChooser();
                files = chooser.showOpenMultipleDialog(primaryStage);
                                
                for(int j = 0; j < files.size(); j++)
                {
                    filesForConvert.add(files.get(j).getAbsolutePath());
                    
                    listString += filesForConvert.get(j) + "\n";
                }
                               
                openFiles.setText(listString);                
            }
        });
        
        clear_files_list.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                                                    
                openFiles.setText("is empty k?");                                  
                listString = "";
                System.out.println(files.size());
                files = null;
            }
        });
        
        pickDirectory.setOnAction(new EventHandler<ActionEvent>() {            
            @Override
            public void handle(ActionEvent event) {
                DirectoryChooser  chooser = new DirectoryChooser();
                File dir_selected =  chooser.showDialog(primaryStage);
                
                if(dir_selected.exists()){
                    dirField.setText(dir_selected.getAbsolutePath());                
                }
                else
                    dirField.setText("Failed to pick folder");                 
            }
        });
        
        convertFiles.setOnAction(new EventHandler<ActionEvent>() 
        {            
            @Override
            public void handle(ActionEvent event)
            {
                System.out.println("Starting to Convert Files to custom Model format");
                System.out.println(filesForConvert);     
                try {
                    FBX_LOADER.open_Fbx("C://Users//Conex//Documents//Projekte//ProjektDaten//bluegem_game_project//game_resource//skeleton_pointing.fbx");
                } catch (IOException ex) {
                    Logger.getLogger(Parsernew.class.getName()).log(Level.SEVERE, null, ex);
                }
        
                //System.out.println(filesForConvert);
                return;
            }
        });
                
        root.getChildren().add(openFile);
        root.getChildren().add(pickDirectory);
        root.getChildren().add(openFiles);
        root.getChildren().add(dirField);
        root.getChildren().add(bar);
        root.getChildren().add(convertFiles);
        root.getChildren().add(clear_files_list);
        
        Scene scene = new Scene(root, width, height);        
        primaryStage.setTitle("FBX ascii/ OBJ  to custom Format parser");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
