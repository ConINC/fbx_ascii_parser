package parsernew;

public class Material
{
    public String color_name;
    public String bump_name;
    public String spec_name;
    public String shader_name;    
    public int shader_index;    
    public float bump_power;
    public float spec_power;
}