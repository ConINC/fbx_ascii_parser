/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parsernew;

import java.util.ArrayList;

/**
 *
 * @author Conex
 */

class Vector3
{
    public float x,y,z;
    
    public Vector3(float x, float y,float z)
    {
        this.x = x; this.y =y; this.z= z;
    }
}

class Vector2
{
    public float x,y;      
    public Vector2(float x, float y)
    {
        this.x = x; this.y =y;
    }
}

class Vector5
{
    public float x,y,z,w,h;
    
    public Vector5(float x, float y, float z, float w, float h)
    {
        this.x = x; this.y =y; this.z = z; this.w = w; this.h = h;
    }
}

public class Vertex_Data
{
    public ArrayList<Vector3> Points;
    public ArrayList<Vector2> Uvs;
    public ArrayList<Vector3> Normals;
        
    public Vertex_Data()
    {
        this.Points = new ArrayList<>();
        this.Normals = new ArrayList<>();
        this.Uvs = new ArrayList<>();
    }
}